import subprocess

from fastapi import FastAPI, UploadFile, File
from tempfile import TemporaryDirectory
from nemo.collections import asr
import os

# загрузка модели
model = 'model.nemo'
asr_model = asr.models.EncDecCTCModel.restore_from(model)

app = FastAPI()  # создание приложения FastAPI


@app.post('/process')
async def process(file: UploadFile = File(...)):
    """Обработка аудиофайлов"""
    content = await file.read()  # чтение файла

    with TemporaryDirectory() as tmpdir:    # создание временной директории
        filename = os.path.join(tmpdir, file.filename)  # путь для сохранения файла
        # сохранение файла
        with open(filename, 'wb') as output_file:
            output_file.write(content)
        # проверка, что указан тип данных
        if file.content_type is None:
            return {
                'ok': False,
                'message': 'Content type is not specified',
            }
        # проверка, что прикреплен аудиофайл
        elif not file.content_type.startswith('audio'):
            return {
                'ok': False,
                'message': 'Content type is not supported',
            }
        # проверка, что файл в wav формате
        elif file.content_type != 'audio/wav':
            filename = convert_to_wav(filename)  # преобразование в wav формат

        transcripts = asr_model.transcribe([filename], batch_size=20)   # распознавание текста

    return {
        'ok': True,
        'text': transcripts[0],
    }


def convert_to_wav(filename):
    """Преобразование файла в формат wav при помощи ffmpeg"""
    name, ext = os.path.splitext(filename)
    new_filename = name + '.wav'
    subprocess.run(['ffmpeg', '-i', filename, new_filename])    # вызов ffmpeg для преобразования 
    return new_filename


