from django.utils import timezone

from django.test import TestCase, Client

from .models import TextNote as Note


class IndexTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Note.objects.create(added_date=timezone.now(), text='test1')
        Note.objects.create(added_date=timezone.now(), text='test2')
        Note.objects.create(added_date=timezone.now(), text='test3')
        Note.objects.create(added_date=timezone.now(), text='test4')
        Note.objects.create(added_date=timezone.now(), text='test5')

    def test_index(self):
        c = Client()
        response = c.get('/')
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.content.decode().count('</li>'), 5)
        self.assertIn('test1', response.content.decode())


class AddNoteTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Note.objects.create(added_date=timezone.now(), text='test1')
        Note.objects.create(added_date=timezone.now(), text='test2')
        Note.objects.create(added_date=timezone.now(), text='test3')
        Note.objects.create(added_date=timezone.now(), text='test4')

    def test_add_note(self):
        c = Client()
        response = c.post('/add_note/', {'content': 'test5'})
        self.assertEquals(response.status_code, 201)
        self.assertEquals(Note.objects.count(), 5)


class UpdateNoteTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Note.objects.create(added_date=timezone.now(), text='test1')
        Note.objects.create(added_date=timezone.now(), text='test2')
        Note.objects.create(added_date=timezone.now(), text='test3')
        Note.objects.create(added_date=timezone.now(), text='test4')

    def test_update_note(self):
        c = Client()
        response = c.post('/update_note/2/', {'content': 'test5'})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Note.objects.get(id=2).content, 'test5')


class DeleteNoteTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Note.objects.create(added_date=timezone.now(), text='test1')
        Note.objects.create(added_date=timezone.now(), text='test2')
        Note.objects.create(added_date=timezone.now(), text='test3')
        Note.objects.create(added_date=timezone.now(), text='test4')

    def test_delete_note(self):
        c = Client()
        response = c.post('/delete_note/1/')
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Note.objects.count(), 3)
