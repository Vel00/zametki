from django.apps import AppConfig


class ZametkiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'zametki'
