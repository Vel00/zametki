from django.urls import path

from .views import index, add_note, update_note, delete_note, add_audio_note, transcribe_note

urlpatterns = [
    path('', index, name='index'),
    path('add_note/', add_note, name='add_note'),
    path('update_note/<int:note_id>/', update_note, name='update_note'),
    path('delete_note/<int:note_id>/', delete_note, name='delete_note'),
    path('add_audio_note/', add_audio_note, name='add_audio_note'),
    path('transcribe_note/<int:note_id>/', transcribe_note, name='transcribe_note'),
]
