from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone
from .models import BaseNote, TextNote, AudioNote
from .transcriber import Transcriber


def index(request):
    """Главная страница приложения"""
    notes = BaseNote.objects.all().order_by("-added_date")  # получение заметок из БД
    return render(request, 'zametki/index.html', {
        'notes': notes,
    })


def add_note(request):
    """Добавление текстовой заметки"""
    content = request.POST['content']   # извлечение из данных запроса содержимого заметки
    TextNote.objects.create(added_date=timezone.now(), text=content)    # создание записи заметки
    return HttpResponse(status=201)


def update_note(request, note_id):
    """Редактирование текста заметки"""
    content = request.POST['content']
    note = BaseNote.objects.get(id=note_id)    # выбор заметки для редактирования
    note.content = content   # обновление её содержимого
    note.save()
    return HttpResponse(status=200)


def delete_note(request, note_id):
    """Удаление заметки"""
    BaseNote.objects.get(id=note_id).delete()   # выбор заметки по id и её удаление
    return HttpResponse(status=200)


def add_audio_note(request):
    """Добавление аудио заметки"""
    # создание заметки, сохранение аудиофайла
    note = AudioNote.objects.create(added_date=timezone.now(), file=request.FILES['file'])

    # распознавание текста
    note.content = Transcriber.instance().get_text(note)
    note.save()

    return HttpResponse(status=201)


def transcribe_note(request, note_id):
    """Принудительное распознавание текста аудио заметки"""
    note = AudioNote.objects.get(id=note_id)
    note.content = Transcriber.instance().get_text(note)
    note.save()

    return HttpResponse(status=200)
