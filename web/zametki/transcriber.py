import requests

from .models import AudioNote


class Transcriber:
    """Класс для взаимодействия с сервисом распознавания текста"""
    __instance = None   # Объявление единственного экземпляра класса

    def __init__(self):
        self.url = "http://127.0.0.1:8888/process/"

    @classmethod
    def instance(cls):
        """Получение экземпляра 'одиночки'."""
        # Создание экземпляра при отсутствии
        if not cls.__instance:
            cls.__instance = cls()

        return cls.__instance

    def get_text(self, audio: AudioNote):
        """Распознавание текста при помощи сервиса"""

        files = {
            'file': (audio.file.name, audio.file.open('rb'), 'audio/webm'),  # подготовка файла к отправке на сервис
        }
        response = requests.post(self.url, files=files)  # запрос к сервису
        data = response.json()

        if data['ok']:
            return data['text']

        return 'Нераспознано'

