import os

from django.db import models
from django.dispatch import receiver
from django.template.loader import get_template
from polymorphic.models import PolymorphicModel


class BaseNote(PolymorphicModel):
    """Базовая модель заметки"""

    added_date = models.DateTimeField()  # дата добавления

    template_name: str  # название шаблона для заметки

    @property
    def content(self) -> str:
        """Получение содержимого заметки"""
        raise NotImplementedError

    @content.setter
    def content(self, content: str):
        """Установка содержимого заметки"""
        raise NotImplementedError

    @property
    def title(self) -> str:
        """Заголовок заметки"""
        return self.content

    @property
    def html(self) -> str:
        """HTML-представление заметки"""
        return get_template(self.template_name).render({'note': self})


class TextNote(BaseNote):
    """Модель текстовой заметки"""
    text = models.CharField(max_length=1000)    # текст заметки

    template_name = 'zametki/note.html'

    @property
    def content(self):
        return self.text

    @content.setter
    def content(self, content: str):
        self.text = content


class AudioNote(BaseNote):
    """Модель аудиозаписи прикрепленной к заметке"""
    file = models.FileField()   # аудиофайл
    transcription = models.CharField(max_length=1000, null=True, blank=True)    # распознанный текст

    template_name = 'zametki/audio_note.html'

    @property
    def content(self):
        return self.transcription

    @content.setter
    def content(self, content: str):
        self.transcription = content


@receiver(models.signals.post_delete, sender=AudioNote)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """Обработчик, который удаляет файл при удалении связанной с ним записи из БД"""
    if instance.file:
        if os.path.isfile(instance.file.path):
            try:
                os.remove(instance.file.path)
            except PermissionError:
                pass
