// Получение cookie из локального хранилища по ключу
function getCookie(key) {
    let pairs = document.cookie.split(";");
    let cookies = {};
    for (let i=0; i<pairs.length; i++){
        let pair = pairs[i].split("=");
        cookies[(pair[0]+'').trim()] = unescape(pair.slice(1).join('='));
    }
    return cookies[key];
}

var current_note = null

// Выбор заметки
function selectNote() {
    $('li').removeClass('active')
    $(this).addClass('active')
    current_note = this.dataset.id
    $('#textarea').val(this.dataset.text)

    // Проверка является ли заметка аудио заметкой
    if (this.dataset.src) {
        let audio = $('audio')[0]
        audio.src = this.dataset.src
        // Отображение аудио элемента
        audio.controls = true
        if (current_note != null && current_note != -1)
            $('#transcribeButton').removeClass('d-none')
        else
            $('#transcribeButton').addClass('d-none')
    } else {
        $('audio')[0].controls = false
        $('#transcribeButton').addClass('d-none')
    }
}

// Делает элементы списка выбираемыми
function prepareLi() {
    $('li').on('click', selectNote)
}

// Вывод ошибки при неудаче запроса
function onFail() {
    if (confirm('Произошла ошибка. Обновить страницу?'))
        location.reload()
}

// Сохранение новой заметки
function addNote() {
    let current = $('li.active')[0]
    if (current.dataset.src) {
        // Объект данных формы для загрузки файла
        let form = new FormData()
        // Добавление файла в форму
        form.append('file', recorder.records[current.dataset.src], 'audio.webm')
        $.ajax({
            url: '/add_audio_note/',
            method: 'post',
            headers: {'X-CSRFToken': getCookie('csrftoken')},
            data: form,
            contentType: false,
            processData: false,
            success: function(data) {
                location.reload()
            },
            error: onFail
        })
    } else {
        let text = $('#textarea').val()
        $.ajax({
            url: '/add_note/',
            method: 'post',
            headers: {'X-CSRFToken': getCookie('csrftoken')},
            data: {content: text},
            success: function(data) {
                location.reload()
            },
            error: onFail
        })
    }
}

// Сохранение отредактированной заметки
function saveNote() {
    let text = $('#textarea').val()
    $.ajax({
        url: `/update_note/${current_note}/`,
        method: 'post',
        headers: {'X-CSRFToken': getCookie('csrftoken')},
        data: {content: text},
        success: function(data) {
            location.reload()
        },
        error: onFail
    })
}

// Удаление выбранной заметки
function deleteNote() {
    $.ajax({
        url: `/delete_note/${current_note}/`,
        method: 'post',
        headers: {'X-CSRFToken': getCookie('csrftoken')},
        success: function(data) {
            location.reload()
        },
        error: onFail
    })
}

// Распознавание выбранной заметки
function transcribeNote() {
    $('#spinner2').removeClass('d-none')
    $.ajax({
        url: `/transcribe_note/${current_note}/`,
        method: 'post',
        headers: {'X-CSRFToken': getCookie('csrftoken')},
        success: function(data) {
            location.reload()
        },
        error: onFail
    })
}

// Добавление заметки в список заметок без сохранения
function onAddNote() {
    let li = $('<li>Новая заметка</li>')
    li.addClass('list-group-item').attr('data-id', -1).attr('data-text', '').on('click', selectNote)
    $('ul').prepend(li)
    li.trigger('click')
}

// Обработка нажатия на кнопку сохранить
function onSaveNote() {
    if (current_note == null) return
    $('#spinner').removeClass('d-none')
    if (current_note == -1) return addNote()

    saveNote()
}

// Обработка нажатия на кнопку удалить
function onDeleteNote() {
    if (current_note == null || current_note == -1) return
    deleteNote()
}

// Обработка нажатия на кнопку распознать
function onTranscribeNote() {
    if (current_note == null || current_note == -1) return
    transcribeNote()
}

// Класс записывающий аудио заметки
class AudioRecorder {
    chunks = [];
    recording = false;
    mediaRecorder;
    records = {};
    constructor(onRecordingStarted, onRecordingFinished) {
        this.onRecordingFinished = onRecordingFinished
        this.onRecordingStarted = onRecordingStarted
        // Получение разрешения на запись аудио
        if (navigator.mediaDevices.getUserMedia) {
             navigator.mediaDevices.getUserMedia({ audio: true }).then(
                this.onSuccess.bind(this),
                this.onError.bind(this)
             );
        }
    }

    // При разрешении использовать микрофон
    onSuccess(stream) {
        this.mediaRecorder = new MediaRecorder(stream);

        this.mediaRecorder.onstop = this.onStop.bind(this)

        this.mediaRecorder.ondataavailable = this.onData.bind(this)

        this.startRecording()
    }

    // Вывод ошибки при неудаче запроса доступа к микрофону
    onError(error) {
        console.log('Error: ' + error)
    }

    // Действия по завершению записи
    onStop(event) {
        // Создание виртуального файла аудио записи
        this.blob = new Blob(this.chunks, { type: this.mediaRecorder.mimeType });
        // Очищение записанных фрагментов
        this.chunks = [];
        // Создание ссылки для прослушивания записи
        this.audioURL = window.URL.createObjectURL(this.blob);
        // Сохранение записи в словарь записей
        this.records[this.audioURL] = this.blob

        // Вызов колбэка на завершение записи при наличии
        if (this.onRecordingFinished)
            this.onRecordingFinished(this)
    }

    // Обработка записываемых данных
    onData(event) {
        this.chunks.push(event.data)
    }

    // Запуск записи
    startRecording() {
        this.mediaRecorder.start()
        this.recording = true

        // Вызов колбэка на запуск записи при наличии
        if (this.onRecordingStarted)
            this.onRecordingStarted()
    }

    // Остановка записи
    stopRecording() {
        this.mediaRecorder.stop()
        this.recording = false
    }
}

var recorder = null

// Изменение цвета микрофона при записи
function onRecordingStarted() {
    $('#mic_btn').removeClass('btn-outline-primary').addClass('btn-danger')
}

// Обработка завершения записи аудио
function onRecordingFinished(recorder) {
    $('#mic_btn').removeClass('btn-danger').addClass('btn-outline-primary')

    let li = $('<li>Новая аудио заметка</li>')
    li.addClass('list-group-item').attr('data-id', -1).attr('data-text', '').attr('data-src', recorder.audioURL).on('click', selectNote)
    $('ul').prepend(li)
    li.trigger('click')
}

// Обработка нажатия на кнопку микрофона
function onMic() {
    if (recorder == null) {
        recorder = new AudioRecorder(onRecordingStarted, onRecordingFinished)
    } else {
        if (recorder.recording)
            recorder.stopRecording()
        else
            recorder.startRecording()
    }
}

$(document).ready(function() {
    prepareLi()
})

